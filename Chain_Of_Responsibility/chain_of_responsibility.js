var  global = {};

global.HelpHandler = function(handler){
	this.handler = handler
};


global.HelpHandler.prototype.HandleHelp = function(){
	this.handler.HandleHelp();
 };


global.widget = function(handler){
	global.HelpHandler.call(this,handler);
};
global.widget.prototype = new global.HelpHandler();

global.widget.prototype.constructor = global.widget;


global.Button = function(handler){
	global.HelpHandler.call(this,handler);
};

global.Button.prototype = new global.widget();

global.Button.prototype.constructor = global.Button;

global.Button.prototype.showHelp = function(){
	console.log("heres the help");
})

global.Button.prototype.HandleHelp = function(){
	if (can_help){
		this.showHelp();
	}
	else{
		global.HelpHandler.prototype.HandleHelp.call(this);
	}

}
