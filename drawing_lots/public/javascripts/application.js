var Lots = Lots || {};

Lots.Entrant = Backbone.Model.extend({});

Lots.EntrantsList = Backbone.Collection.extend({

	model: Lots.Entrant
});

Lots.Entrants = new Lots.EntrantsList(); 

Lots.EntrantView = Backbone.View.extend({

    tagName: 'li',

    initialize: function() {
        _.bindAll(this, 'render');
        this.model.on('change', this.render);
        this.template = _.template($('#entrant-template').html());
    },

    render: function() {
        var renderedContent = this.template(this.model.toJSON());  
        $(this.el).html(renderedContent);
        return this;
    }
});

Lots.EntrantsListView = Backbone.View.extend({

    events: {
        'submit #new_entry':  'addEntrant',
        'click #draw': 'drawWinner',
    },

    initialize: function() {
      _.bindAll(this, 'render');
      this.collection.on('reset', this.render);
      this.template = _.template($('#entrants-list-template').html());
    },

    addEntrant: function(event) {
        event.preventDefault();
        var entrant_name = $('#new_entry_name').val();
        entrant = new Lots.Entrant({name: entrant_name, winner: false});
        this.collection.add(entrant);
        this.render();
    },

    drawWinner: function() {
        shuffledList = _.shuffle(this.collection.models);
        if (shuffledList[0] != undefined) {
        drawnEntrant = _.first(shuffledList);
        }
        //THIS LOOP DELETES THE WINNER FROM THE ENTRANTS LIST SO THEY CAN ONLY BE A WINNER ONCE
        for(var i=0; i<this.collection.models.length; i++){
            if(this.collection.models[i] == drawnEntrant){
                this.collection.models.splice(i,1);
           }
        }       
        drawnEntrant.set({winner: true});
    },

    render: function() {
        $(this.el).html(this.template({}));
        var entries = $('#entries');
        this.collection.each(function(entrant) {
            view = new Lots.EntrantView({ model: entrant });
            entries.append(view.render().el);
        })
        return this;
    }
});

Lots.Router = Backbone.Router.extend({
    
    routes: {
        '': 'home',
    },
    
    initialize: function() {
        this.EntrantsListView = new Lots.EntrantsListView({
            collection: Lots.Entrants
        });
    },
    
    home: function() {
        var container = $('#container');
        container.append(this.EntrantsListView.render().el);
    }
});

$(function() {
    Lots.router = new Lots.Router();
    Backbone.history.start();
});




