require 'rubygems'
require 'sinatra'

get '/entrants' do
  content_type 'application/json'
  send_file 'public/entrants.json'
end