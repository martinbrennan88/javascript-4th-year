var global = {};

global.regularExpression = function(){
};


global.regularExpression.prototype.interpret = function(input){
	if(typeof input === "string")
	{
		input = input.replace(/\s/g, '');
		var stream = new global.stream(input);
		this._inputState = new Array();
		this._inputState.push(stream)
	} 
	else
	{
		 this._inputState = input;
	}
};


global.regularExpression.prototype.repeat = function(){
	return new global.repeat(this);
};

global.regularExpression.prototype.and = function(string){
	return new global.and(this, string.asRegex())
};

global.regularExpression.prototype.or = function(string){
	return new global.and(this, string.asRegex())
};


global.literal = function(string){
	this._components = string.split("");
};

global.literal.prototype = new global.regularExpression();

global.literal.prototype.interpret = function(input){
	global.regularExpression.prototype.interpret.call(this,input);

	var stream;
	var finalState = new Array();
	for (var i = 0; i < this._inputState.length; i++) {
		stream = _.clone(this._inputState[i])
		var characters = stream.nextAvailable(this._components.length);
		if(characters.toString() === this._components.toString()){
			finalState.push(stream);
		}
	}
	return finalState;
};

String.prototype.repeat = function(){
	return new global.repeat(this.valueOf().asRegex());
};


global.repeat = function(expression){
	this._regex = expression;
};
global.repeat.prototype = new global.regularExpression();

global.repeat.prototype.interpret = function(input){
		var finalState = new Array();
		var astate = this._regex.interpret(input);
		while(astate.length >= 1){
			finalState = finalState.concat(astate);
			astate = this._regex.interpret(astate);
		}
		return finalState;
};

String.prototype.or = function(string){
	return new global.or(this.valueOf().asRegex(), string.asRegex());
};


global.or = function(expression, expression2){
	this._regex1 = expression;
	this._regex2 = expression2;
};
global.or.prototype = new global.regularExpression();

global.or.prototype.interpret = function(input){
	var finalState = new Array();
	finalState = finalState.concat(this._regex1.interpret(input));
	finalState = finalState.concat(this._regex2.interpret(input));
	return finalState;
};

String.prototype.and = function(string){
	return new global.and(this.valueOf().asRegex(), string.asRegex());
};


global.and = function(expression, expression2){
	this._regex1 = expression;
	this._regex2 = expression2; 
};

global.and.prototype = new global.regularExpression();

global.and.prototype.interpret = function(inputState){
	var finalState = new Array();
	finalState = finalState.concat(this._regex2.interpret(this._regex1.interpret(inputState)));
	return finalState;
};


String.prototype.asRegex = function(){
	return new global.literal(this);
};


global.stream = function(string){
	this._string = string;
	this._counter = 0;
};

global.stream.prototype.nextAvailable = function(num){
	var character_array = this._string.split('');
	var characterset = character_array.slice(this._counter, this._counter + num);
	this._counter = this._counter + num;
	return characterset;
};