global = {}

global.VisualComponent = function(){};

global.VisualComponent.prototype.draw = function(){};


global.Textview = function(){};

global.Textview.prototype = new global.VisualComponent();

global.Decorator = function(){};

global.Decorator.prototype = new global.VisualComponent();

global.Decorator.prototype.draw = function(){
	console.log("draw something " + this.name);
};

global.BorderDecorator = function(name){
	this.name = name;
};


global.BorderDecorator.prototype.drawBorder = function(){
	console.log("then draw the border")
};


global.BorderDecorator.prototype.draw = function(){
	global.Decorator.prototype.draw.call(this);
	this.drawBorder();
};


global.ScrollDecorator = function(){};


global.ScrollDecorator .prototype.scrollTo = function(){
	console.log("then scroll here")
};


global.ScrollDecorator .prototype.draw = function(){
	global.Decorator.prototype.draw.call(this);
	this.scrollTo();
};